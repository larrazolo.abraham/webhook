import json
from datetime import datetime

import uvicorn
from fastapi import FastAPI, Request

app = FastAPI()


@app.post("/gitlab_webhook")
async def get_webhook(info: Request):
    req_info = await info.json()
    print(f"Received: {req_info}")
    now = datetime.now()
    current_time = now.strftime("%H_%M_%S")
    with open(f"{current_time}.json", "w") as file:
        json.dump(req_info, file)
    return {"status": "SUCCESS", "data": req_info}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
